package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_EXISTS = "定義ファイルが存在しません";
	private static final String FILE_NUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String FIGURE_LENGTH_OVER = "合計金額が10桁超えました";
	private static final String FILE_FORMAT_ERROR = "ファイルのフォーマットが不正です";
	private static final String BRANCH_MAP_KEY_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_MAP_KEY_ERROR ="の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が渡されているか 
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		String branchRegex = "^[0-9]{3}$";
		String commodityRegex = "^[A-Za-z0-9]{8}$";

		String branch = "支店";
		String commodity = "商品";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchRegex, branchNames, branchSales, branch)) {
			return;
		}
		//商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityRegex, commodityNames, commoditySales, commodity)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {

			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd+$")) {
				//売上ファイルの条件に当てはまったものだけをrcdFilesリストに追加する。
				rcdFiles.add(files[i]);

			}

		}
		Collections.sort(rcdFiles);
		//売上ファイルが連番になっているかの確認。 

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NUMBER_ERROR);
				return;
			}
		}

		//売上ファイルの読込
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {

			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				List<String> list = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					list.add(line);
				}

				//売上ファイルのフォーマットが合っているかの確認 
				if (list.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_FORMAT_ERROR);
					return;
				}

				//売上金額が数字なのかを確認
				if (!list.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//Mapにキーが存在するかの確認 
				if (!branchSales.containsKey(list.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_MAP_KEY_ERROR);
					return;
				}
				//Mapにキーが存在するか確認
				if (!commoditySales.containsKey(list.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_MAP_KEY_ERROR);
					return;
				}

				long fileSale = Long.parseLong(list.get(2));
				Long branchSaleAmount = branchSales.get(list.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(list.get(1)) + fileSale;

				//桁数が超えていないかの確認 
				if (branchSaleAmount >= 10000000000L) {
					System.out.println(FIGURE_LENGTH_OVER);
					return;
				}
				if (commoditySaleAmount >= 10000000000L) {
					System.out.println(FIGURE_LENGTH_OVER);
					return;
				}

				branchSales.put(list.get(0), branchSaleAmount);
				commoditySales.put(list.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String regex, Map<String, String> names,
			Map<String, Long> sales, String initialName) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//支店定義,、商品定義ファイルが存在しない処理 
			if (!file.exists()) {
				System.out.println(initialName + FILE_NOT_EXISTS);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。
				String[] items = line.split(",");

				//ファイルフォーマット不正処理 
				if ((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(initialName + FILE_INVALID_FORMAT);
					return false;

				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L); //最初に0をしておかないと空白のままになってしまう為。

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();

			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}